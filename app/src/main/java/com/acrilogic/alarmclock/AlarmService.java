package com.acrilogic.alarmclock;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Service for running alarm
 */
public class AlarmService extends Service {
    private Handler handler = new Handler();

    Set<String> daysOfWeek;
    String musicPackage;
    int audioLevel;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final Resources resources = getResources();

        musicPackage = sharedPreferences.getString(resources.getString(R.string.key_alarm_music_app), "com.google.android.music");
        audioLevel = sharedPreferences.getInt(resources.getString(R.string.key_alarm_volume), 0);
        daysOfWeek = sharedPreferences.getStringSet(resources.getString(R.string.key_alarm_days), new HashSet<String>());

        int alarmTime = sharedPreferences.getInt(resources.getString(R.string.key_alarm_time), 480);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        if (calendar.get(Calendar.HOUR) * 60 + calendar.get(Calendar.MINUTE) >= alarmTime) {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
        }
        calendar.set(Calendar.HOUR, alarmTime / 60);
        calendar.set(Calendar.MINUTE, alarmTime % 60);

        handler.postDelayed(checkAlarmRunnable, calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis());

        Log.d("tes", "Starts in " + String.valueOf(calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()));
        return super.onStartCommand(intent, flags, startId);
    }

    Runnable checkAlarmRunnable = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(checkAlarmRunnable, 1000 * 60 * 60 * 24);

            if (daysOfWeek.contains(String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)))) {
                startAlarm();
            }
        }
    };

    public void startAlarm() {
        ((AudioManager) getSystemService(Context.AUDIO_SERVICE)).setStreamVolume(AudioManager.STREAM_MUSIC, audioLevel, 0);

        final KeyEvent downEvent = new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY);
        final KeyEvent upEvent = new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PLAY);

        Intent mediaEvent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        mediaEvent.putExtra(Intent.EXTRA_KEY_EVENT, downEvent);
        mediaEvent.setPackage(musicPackage);
        sendBroadcast(mediaEvent);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mediaEvent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                mediaEvent.putExtra(Intent.EXTRA_KEY_EVENT, upEvent);
                mediaEvent.setPackage(musicPackage);
                sendBroadcast(mediaEvent);

            }
        }, 100);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(checkAlarmRunnable);
        super.onDestroy();
    }
}
