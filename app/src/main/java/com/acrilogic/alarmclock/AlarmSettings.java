package com.acrilogic.alarmclock;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TimePicker;

/**
 * Settings for individual alarm
 */
public class AlarmSettings extends PreferenceActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new AlarmSettingsFragment())
                .commit();
    }

    @Override
    protected void onDestroy() {
        Intent intent = new Intent(this, AlarmService.class);
        stopService(intent);
        startService(intent);

        super.onDestroy();
    }

    public static class AlarmSettingsFragment extends PreferenceFragment {
        int currentAlarmTime;
        int currentAlarmVolume;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            final Resources resources = getResources();

            currentAlarmTime = sharedPreferences.getInt(resources.getString(R.string.key_alarm_time), 480);
            currentAlarmVolume = sharedPreferences.getInt(resources.getString(R.string.key_alarm_volume), 0);

            addPreferencesFromResource(R.xml.preferences);

            Preference timePreference = findPreference(resources.getString(R.string.key_alarm_time));
            String minuteString = String.valueOf(currentAlarmTime % 60);
            if (currentAlarmTime % 60 < 10) {
                minuteString = "0" + minuteString;
            }
            timePreference.setSummary(String.valueOf(currentAlarmTime / 60) + ":" + minuteString);
            timePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(final Preference preference) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String minuteString = String.valueOf(minute);
                            if (minute < 10) {
                                minuteString = "0" + minuteString;
                            }
                            String alarmString = String.valueOf(hourOfDay) + ":" + minuteString;
                            preference.setSummary(alarmString);

                            currentAlarmTime = hourOfDay * 60 + minute;
                            SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
                            prefs.putInt(resources.getString(R.string.key_alarm_time), currentAlarmTime);
                            prefs.apply();

                        }
                    }, currentAlarmTime / 60, currentAlarmTime % 60, true);
                    timePickerDialog.show();

                    return false;
                }
            });
        }
    }
}
